use std::sync::Arc;

use axum::{
  extract::{Json, Path, Query},
  http::StatusCode,
};
use serde::Serialize;

use crate::api;
use crate::state;

#[derive(Serialize, Debug)]
pub struct Coords {
  latitude: f32,
  longitude: f32,
}

impl From<api::Geometry> for Coords {
  fn from(geometry: api::Geometry) -> Self {
    Self {
      longitude: *geometry.coordinates.get(0).unwrap_or(&0f32),
      latitude: *geometry.coordinates.get(1).unwrap_or(&0f32),
    }
  }
}

#[derive(Serialize, Debug)]
pub struct SearchResult {
  place_name_en: String,
  coords: Coords,
  address: String,
  city: String,
  state: String,
  category: String,
  text: String,
}

pub type SearchResults = Vec<SearchResult>;

impl From<api::Feature> for SearchResult {
  fn from(feature: api::Feature) -> Self {
    Self {
      place_name_en: feature.place_name_en,
      coords: Coords::from(feature.geometry),
      address: feature.properties.address,
      city: get_context_value_for_key(feature.context.iter(), "place"),
      state: get_context_value_for_key(feature.context.iter(), "state"),
      category: feature.properties.category,
      text: feature.text,
    }
  }
}

fn get_context_value_for_key<'a, I>(mut contexts: I, key: &str) -> String
where
  I: Iterator<Item = &'a api::Context>,
{
  contexts
    .find(|context| context.is(key))
    .map(|context| context.text_en.clone())
    .unwrap_or_else(|| "".to_owned())
}

pub async fn search(
  Path(search_input): Path<String>,
  Query(params): Query<api::MapBoxParams>,
  state: Arc<state::State>,
) -> Result<Json<SearchResults>, StatusCode> {
  println!("Search input = {}, params = {:?}", search_input, params);
  match api::query_mapbox(
    search_input,
    api::WrappedParams::new(params, state.api_key.to_owned()),
  )
  .await
  {
    Ok(result) => Ok(
      result
        .features
        .into_iter()
        .map(SearchResult::from)
        .collect::<SearchResults>()
        .into(),
    ),
    Err(e) => {
      eprintln!("Error while querying MapBox: {}", e);
      Err(StatusCode::from_u16(500).unwrap())
    }
  }
}
