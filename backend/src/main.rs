use std::sync::Arc;

use axum::{
  body::{boxed, Full},
  handler::Handler,
  http::Uri,
  response::{IntoResponse, Response},
  routing::get,
  Router,
};
use dotenv::dotenv;
use rust_embed::RustEmbed;

mod api;
mod search;
mod state;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
  dotenv()?;
  let state = Arc::new(state::State {
    api_key: std::env::var("API_KEY")?,
  });
  // build our application with a single route
  let app = Router::new()
    .route("/", get(index_handler))
    .route("/index.html", get(index_handler))
    .route(
      "/api/search/:search_input",
      get(move |path, query| search::search(path, query, state.clone())),
    )
    .fallback(static_handler.into_service());

  // run it with hyper on localhost:3000
  axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
    .serve(app.into_make_service())
    .await
    .unwrap();
  Ok(())
}

async fn index_handler() -> impl IntoResponse {
  static_handler("/index.html".parse::<Uri>().unwrap()).await
}

async fn static_handler(uri: Uri) -> impl IntoResponse {
  StaticFile(uri.path().trim_start_matches('/').to_string())
}

#[derive(RustEmbed)]
#[folder = "public/"]
struct Asset;

pub struct StaticFile<T>(pub T);

impl<T> IntoResponse for StaticFile<T>
where
  T: Into<String>,
{
  fn into_response(self) -> Response {
    let path = self.0.into();

    println!("Searching for file {} under public/", path);
    match Asset::get(path.as_str()) {
      Some(content) => {
        let body = boxed(Full::from(content.data));
        let mime = mime_guess::from_path(path).first_or_octet_stream();
        Response::builder()
          .status(200)
          .header("Content-Type", mime.as_ref())
          .body(body)
          .unwrap()
      }
      None => Response::builder()
        .status(404)
        .body(boxed(Full::from("404")))
        .unwrap(),
    }
  }
}
