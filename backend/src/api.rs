use serde::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize, Debug)]
pub struct Geometry {
  pub coordinates: [f32; 2],
}

#[derive(Deserialize, Debug)]
pub struct Properties {
  pub address: String,
  pub category: String,
}

#[derive(Deserialize, Debug)]
pub struct Context {
  id: String,
  pub text_en: String,
}

impl Context {
  pub fn is(&self, value: &str) -> bool {
    self.id.starts_with(value)
  }
}

#[derive(Deserialize, Debug)]
pub struct Feature {
  pub properties: Properties,
  pub text: String,
  pub place_name_en: String,
  pub geometry: Geometry,
  pub context: Vec<Context>,
}

#[derive(Deserialize, Debug)]
pub struct MapBoxResult {
  pub features: Vec<Feature>,
}

#[derive(Deserialize, Debug)]
pub struct MapBoxParams {
  fuzzy_match: String,
  language: String,
  limit: String,
  longitude: String,
  latitude: String,
}

pub struct WrappedParams {
  params: MapBoxParams,
  access_token: String,
}

impl WrappedParams {
  pub fn new(params: MapBoxParams, access_token: String) -> Self {
    Self {
      params,
      access_token,
    }
  }
}

impl From<WrappedParams> for HashMap<String, String> {
  fn from(params: WrappedParams) -> Self {
    HashMap::from([
      ("access_token".to_owned(), params.access_token),
      ("limit".to_owned(), params.params.limit),
      (
        "proximity".to_owned(),
        format!("{},{}", params.params.longitude, params.params.latitude),
      ),
      ("fuzzy_match".to_owned(), params.params.fuzzy_match),
      ("language".to_owned(), params.params.language),
    ])
  }
}

const MAPBOX_API: &str = "https://api.mapbox.com/geocoding/v5/mapbox.places/";

pub async fn query_mapbox(
  search_input: String,
  params: WrappedParams,
) -> Result<MapBoxResult, Box<dyn std::error::Error>> {
  let url = reqwest::Url::parse(MAPBOX_API)
    .expect("Hardcoded URL is valid")
    .join(&urlencoding::encode(&format!("{}.json", search_input)))?;
  let url = reqwest::Url::parse_with_params(url.as_str(), HashMap::from(params))?;
  println!("Querying: {}", url);
  Ok(
    reqwest::get(url.as_str())
      .await?
      .json::<MapBoxResult>()
      .await?,
  )
}
