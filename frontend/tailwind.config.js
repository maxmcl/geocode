module.exports = {
  content: [
    "./public/index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  jit: true,
  theme: {
    extend: {},
  },
  plugins: [],
}
